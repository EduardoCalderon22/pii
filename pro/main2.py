# coding: utf-8
#main.py


from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout




class Calculator(GridLayout):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.bl = BoxLayout()
		self.add_widget(self.bl)


class Main_Window(App):
	def build(self):
		return Calculator()

Main_Window().run()
